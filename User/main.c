#include "led.h"
#include "delay.h"
#include "key.h"
#include "sys.h"
#include "lcd.h"
#include "beep.h"
#include "usart.h"
#include "24cxx.h"
#include "touch.h"
#include "remote.h"

#include "usart3.h"
#include "wifiSta.h"

// ---------

#include "draw_service.h"
#include "snakes_cluster_service.h"
#include "draw_service.h"
#include "food_service.h"
#include "draw_controller.h"
#include "network_service.h"
#include "malloc.h"
#include "game_process_controller.h"
#include "timer.h"

#include "message_controller.h"

/************************************************
 ALIENTEK精英STM32开发板嵌入式大作业--贪吃蛇   
 开发人员：权观宇(2015210402036) 沈丹杰(2015210402057)
 启动时间：2018.5
************************************************/


void pt_snakes(void) {
    unsigned char i = 0;
    SNAKE_NODE *p = NULL;
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        if (snakes[i] == NULL) continue;
        printf("Snake id:%d direc:%d\t", i, snakes[i]->direction);
        p = snakes[i]; //使p1指向第一个结点
        while (p != NULL) {
            printf("(%d,%d)\t", p->x, p->y);
            p = p->next;
        }
        printf("\r\n");
    }
    printf("\r\n\r\n");
}

//void pt_mem_size(char *str) {
//    int size = 1;
//    char *ppp;
//
//    while (1) {
//        ppp = (char *) mymalloc(SRAMIN, sizeof(char) * size);
//        if (ppp == NULL) break;
//        else myfree(SRAMIN, ppp);
//        size++;
//    }
//    printf("%s remaining space: %d byte \r\n\r\n", str, size);
//    size = 1;
//}

void testDataCreate(int n) {
    FOOD_NODE *f1 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f2 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f3 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f4 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN);
    unsigned char data[63] = {
            0x01, 0x00, 0x01, 0x00, 0x0A, 0x01, 0x02,
            0x02, 0x00, 0x01, 0x00, 0x0A, 0x01, 0x02,
            0x03, 0x00, 0x01, 0x00, 0x0A, 0x01, 0x02,

            0x02, 0x00, 0x02, 0x00, 0x08, 0x02, 0x00,
            0x02, 0x00, 0x03, 0x00, 0x08, 0x02, 0x00,
            0x02, 0x00, 0x04, 0x00, 0x08, 0x02, 0x00,

            0x05, 0x00, 0x06, 0x00, 0x02, 0x03, 0x03,
            0x04, 0x00, 0x06, 0x00, 0x02, 0x03, 0x03,
            0x04, 0x00, 0x05, 0x00, 0x02, 0x03, 0x01
    };
    PLAYER_ID = 3;
    initFlag = 0x01;

    init_snakes(data, 63);

    pt_snakes();

    // 测试绘制
    // 填充食物
    f1->x = 5;
    f1->y = 3;
    f1->colorType = 6;

    f2->x = 6;
    f2->y = 6;
    f2->colorType = 6;

    f3->x = 3;
    f3->y = 9;
    f3->colorType = 6;

    f4->x = 300;
    f4->y = 300;
    f4->colorType = 6;

    add_food_head(f1);
    add_food_head(f2);
    add_food_head(f3);
    add_food_head(f4);

//    reader_screen();
//    for (i = 0; i < 2; ++i) {
//        refresh_snakes();
//        eat_grow();
//        judge_dead();
//        pt_snakes();
//        reader_screen();
//    }



//    clean_snakes();
//    free_food();
//    pt_mem_size("End test");
}


void test_draw(void) {
    set_draw_context(30, 20);
    draw_background();
    draw_point(30, 20, 6);
}

void foodFreeTest(int n) {
    FOOD_NODE *f1 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f2 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f3 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN),
            *f4 = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN);
    FOOD_NODE *p;
    initFlag = 0x01;
    f1->x = 5;
    f1->y = 3;
    f1->colorType = 6;

    f2->x = 6;
    f2->y = 6;
    f2->colorType = 6;

    f3->x = 3;
    f3->y = 9;
    f3->colorType = 6;

    f4->x = 300;
    f4->y = 300;
    f4->colorType = 6;

    add_food_head(f1);
    add_food_head(f2);
    add_food_head(f3);
    add_food_head(f4);
    p = foods;
    while (p != NULL) {
        printf("food x:%d, y:%d\r\n", p->x, p->y);
        p = p->next;
    }

//    pt_mem_size("before free");
    free_food();
    clean_snakes();
    //pt_mem_size("after free");
    printf("%d End\r\n\r\n", n);

}

void testNet(void) {
//    testDataCreate();
    start_game();
    //监听消息 判断是否初始化
    while (1) {
        listen_message_and_operator();

        if (initFlag == 0x01) {
            //初始化后开始游戏
            reader_screen();
            break;
        }
    }
    pt_snakes();

    clean_snakes();
    free_food();
//    update_game();
//    dead_game(15);

}
void pwdLED(void){
    u16 led0pwmval=0;
    u8 dir=1;
    while(1)
    {
        delay_ms(10);
        if(dir)led0pwmval++;
        else led0pwmval--;

        if(led0pwmval>300)dir=0;
        if(led0pwmval==0)dir=1;
        TIM_SetCompare2(TIM3,led0pwmval);
    }
}
/**
主函数
*/
int main(void) {

    short turnScore;//本轮游戏分数
    delay_init();             //延时函数初始化
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置中断优先级分组为组2：2位抢占优先级，2位响应优先级
    uart_init(115200);        //串口初始化为115200
    LED_Init();                 //LED端口初始化
    LCD_Init();
    // 设置横屏
    LCD_Display_Dir(1);

    KEY_Init();
    BEEP_Init();
    Remote_Init();            //红外接收初始化
    tp_dev.init();
    //printf_init();
    usart3_init(115200);        //初始化串口3
    tp_dev.init();
    //printf_wifiTest();
    //wifi模块测试Demo
//    testSTA();

    // -------------------- 配置模式 ---------------------
    //检查WIFI模块是否在线
    checkModuleOn();

    // UDP 配置
    settingUDPMod();

    // 连接路由器
    connectAP("516", "wifiin516");

    // 配置UDO服务器
    configUDPServer("172.17.64.154", "8086");
    // ------------------- 配置模式结束 -------------------

//    pt_mem_size();
    //start_game_process();

//    while (1) {
//        start_game_process();
//        LED0 = !LED0;
//    }

    TIM3_Int_Init(9,719); // 1Khz的计数频率,计数到10

    while (1) {
        //首页绘制
        draw_welcome_page(DRAW_PAGE_TYPE_START,0x00);
        //开始游戏流程化
        turnScore = start_game_process();

        draw_welcome_page(DRAW_PAGE_TYPE_END, turnScore);
    }


    //test_draw();
    // 制造测试数据
//    testDataCreate(n);
//    pt_mem_size("Total check");
//    while (1) {
//        testNet();
//        //pt_mem_size("before run");
////        test_snake_cluster(n++);
//        //pt_mem_size("after run");
////        foodFreeTest(n++);
//        LED0 = !LED0;
//        delay_ms(500);
//    }
//    set_draw_context(270, 280);
//    draw_background();

    //连接服务器测试
    //TODO
    //绘制欢迎页
    //welcomePage();

}


