#ifndef __DRAW_SERVICE__
#define __DRAW_SERVICE__

#include "lcd.h"
#include "context.h"



/**
 * 根据传入坐标，初始化座标转换参数
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 */
void set_draw_context(unsigned short int x, unsigned short int y);

/**
 * 在地图上绘制点，该步骤将会转换坐标为实际屏幕坐标
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 * @param colorType 颜色类型
 */
void draw_point(unsigned short int x, unsigned short int y, unsigned char colorType);

/**
 * 绘制空心矩形
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 * @param colorType 颜色类型
 */
void draw_box(unsigned short int x, unsigned short int y, unsigned char colorType);

/**
 * 绘制T形状的斑纹
 * @param drawNode 绘制的蛇节点
 */
void draw_stripe(SNAKE_NODE drawNode);

/**
 * 绘制蛇节点
 * @param drawNode 绘制的蛇节点
 */
void draw_node(SNAKE_NODE drawNode);

/**
 * 根据颜色类型获取16bit的颜色值 5/6/5
 * @param type 颜色类型
 * @return 565颜色值
 */
unsigned short int chooseColor(unsigned char type);

/**
 * 绘制背景
 */
void draw_background(void);

#endif
