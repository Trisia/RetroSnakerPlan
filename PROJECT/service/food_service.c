#include "food_service.h"

#include "context.h"
#include "malloc.h"
#include "usart.h"
#include <string.h>

/**
 * 设置地图食物
 * @param data 解析数据
 * @param length 数据长度
 */
void set_foods(unsigned char *data, int length) {
    unsigned short offset = 0;
    FOOD_NODE *nodePoint;
    while (offset < length) {
        // 申请内存区域
        nodePoint = (FOOD_NODE *) mymalloc(SRAMIN, FOOD_NODE_LEN);
        // 拷贝到分配的空间中
        memcpy(nodePoint, data + offset, FOOD_NODE_SIZE);
//        printf("Set food >> x: %d,y: %d\r\n",nodePoint->x, nodePoint->y);
        add_food_head(nodePoint);
        // 增加偏移量
        offset += FOOD_NODE_SIZE;
    }
}


/**
 * 添加食物节点
 * @param head
 * @param node
 * @return
 */
void add_food_head(FOOD_NODE *node) {
    FOOD_NODE *p = NULL;
    // 判断失误列表中是否为空
    if (foods == NULL) {
        foods = node;
        foods->next = NULL;
        return;
    }
    p = foods;
    //一直找到最后一个结点
    while (p->next != NULL) {
        p = p->next;
    }
    p->next = node;
    node->next = NULL;
}


/**
 * 根据坐标删除食物节点，如果有食物，则返回食物节点
 * @param x
 * @param y
 * @return 被删除的食物或NULL
 */
FOOD_NODE *remove_food_node(short x, short y) {
    FOOD_NODE *prev;
    FOOD_NODE *current;
    prev = foods;
    current = (foods == NULL) ? NULL : foods->next;

    if (prev->x == x && prev->y == y) {
        foods = current;
        prev->next = NULL;
        return prev;
    } else {
        while (current != NULL) {
            if (current->x == x && current->y == y) {
                prev->next = current->next;
                current->next = NULL;
                return current;
            } else {
                prev = current;
                current = current->next;
            }
        }
    }
    return NULL;
}


/**
 * 释放所有食物信息
 */
void free_food() {
    FOOD_NODE *pt = NULL;

    //未初始化则直接返回
    if (initFlag == 0x00) {
        return;
    }

    //清空所有食物节点
    while (foods != NULL) {
        pt = foods;
        foods = foods->next;
        myfree(SRAMIN, pt);
    }
    //清空吃掉食物节点
    while (ateFoods != NULL) {
        pt = ateFoods;
        ateFoods = ateFoods->next;
        myfree(SRAMIN, pt);
    }
    ateFoods = NULL;

}


/**
 * 玩家蛇吃掉食物，如果是本玩家吃掉的食物
 * 从地图链表中移到被吃掉的食物链表中(放前面),否则直接释放空间
 * @param id 玩家ID
 * @param x
 * @param y
 * @return 是否吃了 0,没有；1表示吃了
 */
unsigned char player_eat_food(unsigned char id, short x, short y) {
    FOOD_NODE *eat;
    //未初始化则直接返回
    if (initFlag == 0x00) {
        return 0;
    }
    //从所有食物链表中抽出此节点，并返回
    eat = remove_food_node(x, y);
    // 为空说明没有吃掉，直接返还
    if (eat == NULL) {
        return 0;
    }

    if (id == PLAYER_ID) {
        //添加进入被吃掉的食物链表，并放置在头结点的后一个
        eat->next = ateFoods;
        ateFoods = eat;
    } else {
        myfree(SRAMIN, eat);
    }
    return 1;
}

/**
 * 获取吃掉食物的组装数据
 * @param content 传出数据内容
 * @param len 传出数据长度
 */
void getAteFoodsData(char **content, unsigned int *len) {
    int size, i, nodes;
    int offset = 0;
    FOOD_NODE *temp = ateFoods;
    char *data;
    //获取食物节点数
    for (i = 0; temp != NULL; temp = temp->next) {
        ++i;
    }
    nodes = i;
    //计算总长度
    size = i * FOOD_NODE_SIZE;
    //分配空间
    data = (char *) mymalloc(SRAMIN, size);
    //重新指向头指针
    temp = ateFoods;
    for (i = 0; i < nodes; ++i) {
        memcpy(data + offset, temp, FOOD_NODE_SIZE);
        printf("Ate food: x: %d, y: %d\r\n", temp->x, temp->y);
        offset += FOOD_NODE_SIZE;
        temp = temp->next;
    }
    //返回值赋值
    *content = data;
    *len = size;
}

