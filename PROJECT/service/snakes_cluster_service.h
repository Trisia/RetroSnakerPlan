#ifndef __SNAKES_CLUSTER_SERVICE__
#define __SNAKES_CLUSTER_SERVICE__

#include "context.h"

/**
 * 解析蛇群信息，存储蛇群,
 * 并设置 initFlag = 0x01。
  *
  * @param data 数据源
  * @param length 数据长度
  */
void init_snakes(unsigned char *data, int length);

/**
 * 清空蛇群信息
 */
void clean_snakes(void);

/**
 * 更新蛇群信息，调用 move_snake ,更新每条蛇信息
 */
void refresh_snakes(void);

/**
 * 检查蛇群是否有吃掉食物,吃掉食物则对相应蛇增加长度，
 * 并调用 player_eat_food 消除地图上的食物
 */
void eat_grow(void);

/**
 * 获取玩家蛇头指针
 * @return 本玩家的蛇头指针
 */
SNAKE_NODE *get_player_snake(void);


/**
 * 检查每条蛇是否死亡：
 * i.  判断是否撞墙,返还 -1
 * ii. 判断是否触碰到其他蛇身体死亡时，则置空蛇信息，再判断是否是本玩家:
 *      是 - 返回杀了本玩家的玩家ID；
 *      不是 - 返回: 0x00
 * @return 死亡类型
 */
short judge_dead(void);



#endif //RETROSNAKERPLAN_SNAKES_CLUSTER_SERVICE_H
