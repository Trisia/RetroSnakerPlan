#ifndef __FOOD_SERVICE_H
#define __FOOD_SERVICE_H	 

#include "context.h"

/**
 * 设置地图食物
 * @param data 解析数据
 * @param length 数据长度
 */
void set_foods(unsigned char * data, int length);

/**
 * 添加食物节点
 * @param head
 * @param node
 * @return
 */
void add_food_head(FOOD_NODE *node);

/**
 * 根据坐标删除食物节点，如果有食物，则返回食物节点
 * @param x
 * @param y
 * @return 被删除的食物或NULL
 */
FOOD_NODE *remove_food_node(short x, short y);

/**
 * 释放所有食物信息
 */
void free_food(void);

/**
 * 玩家蛇吃掉食物，如果是本玩家吃掉的食物
 * 从地图链表中移到被吃掉的食物链表中(放前面),否则直接释放空间
 * @param id 玩家ID
 * @param x
 * @param y
 * @return 是否吃了 0,没有，1表示吃了
 */
unsigned char player_eat_food(unsigned char id, short x, short y);

/**
 * 获取吃掉食物的组装数据
 * @param content 传出数据内容
 * @param len 传出数据长度
 */
void getAteFoodsData(char **content, unsigned int *len);


#endif


