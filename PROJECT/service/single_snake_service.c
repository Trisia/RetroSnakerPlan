#include "single_snake_service.h"

#include "malloc.h"
#include <string.h>

/**
 * 检查某个结点是否存在于某条蛇内部
 * @param head 蛇头指针
 * @param node 检查节点
 * @return 在/不在 （1/0）
 */
unsigned char checkIsIn(SNAKE_NODE *head, SNAKE_NODE node) {
    SNAKE_NODE *p = NULL;
    // 如果头结点为空那么直接使用加入的新节点作为头部返还
    if (head == NULL) {
        return 0;
    }
    p = head; //使p1指向第一个结点

    while (p != NULL) {
        if ((p->x == node.x) && (p->y == node.y)) {
            return 1;
        }
        p = p->next;
    }
    return 0;
}

/**
 * 增长蛇
 * @param head 蛇的头部
 * @return 备增长的蛇头
 */
SNAKE_NODE *grow_snake(SNAKE_NODE *head) {
    SNAKE_NODE *newNode, *p = NULL;
    short dx = 0, dy = 0;
    if (head == NULL) {
        return NULL;
    }
    p = head; //使p1指向第一个结点
    // 寻找尾节点
    while (p->next != NULL) {
        p = p->next;
    }
    newNode = (SNAKE_NODE *) mymalloc(SRAMIN, SNAKE_NODE_LEN);

    switch (p->direction) {
        case DIRECTION_UP:
            dy = 1;
            break;
        case DIRECTION_DOWN:
            dy = -1;
            break;
        case DIRECTION_LEFT:
            dx = 1;
            break;
        case DIRECTION_RIGHT:
            dx = -1;
            break;
    }
    newNode->x = p->x + dx;
    newNode->y = p->y + dy;
    newNode->direction = p->direction;
    newNode->colorType = p->colorType;
    newNode->id = p->id;
    newNode->next = NULL;
    // 接上新节点
    p->next = newNode;

    return head;
}

/**
 * 增加蛇的节点，如果head为NULL则创建一个新的节点的头指针
 * @param head 头结点
 * @param node 需要插入的节点
 * @return 头结点
 */
SNAKE_NODE *add_snake_node(SNAKE_NODE *head, SNAKE_NODE *node) {
    SNAKE_NODE *p0, *p1 = NULL;
//    int i = 0;
    // 如果头结点为空那么直接使用加入的新节点作为头部返还
    if (head == NULL) {
        node->next = NULL;
        return node;
    }
    p1 = head;//使p1指向第一个结点
    p0 = node;//p0指向要插入的结点
//    printf("add_snake_node while start\r\n");

    while (p1->next != NULL) {
        p1 = p1->next;
//        printf("times: %d", ++i);
    }
//    printf("add_snake_node while end\r\n");

    p1->next = p0;
    p0->next = NULL;
    return head;
}


/**
 * 移动蛇
 * 蛇头按照方向移动，身体则移动到前驱
 * @param head
 */
void move_snake(SNAKE_NODE *head) {

    SNAKE_NODE *pNode = head;
    unsigned short previous_x, previous_y, temp_x, temp_y;//前驱的坐标和临时交换坐标
    unsigned char previous_direct, temp_direct;
    //头为空则直接返回
    if (head == NULL) {
        return;
    }
    //记录移动方向

    //首先移动第一个节点
    previous_x = pNode->x;
    previous_y = pNode->y;
    previous_direct = pNode->direction;

    if(pNode-> id == PLAYER_ID){
        beforeDirection = previous_direct;
    }


    switch (previous_direct) {
        case DIRECTION_UP: {
            pNode->y -= 1;
            break;
        }
        case DIRECTION_DOWN: {
            pNode->y += 1;
            break;
        }
        case DIRECTION_LEFT: {
            pNode->x -= 1;
            break;
        }
        case DIRECTION_RIGHT: {
            pNode->x += 1;
            break;
        }
    }

    while (pNode->next != NULL) {
        pNode = pNode->next;

        temp_x = pNode->x;
        temp_y = pNode->y;
        temp_direct = pNode->direction;

        pNode->x = previous_x;
        pNode->y = previous_y;
        pNode->direction = previous_direct;

        previous_x = temp_x;
        previous_y = temp_y;
        previous_direct = temp_direct;
    }
}


/**
 * 改变蛇的方向
 * @param head
 * @param direction
 */
void change_snake_direction(SNAKE_NODE *head, unsigned char direction) {
    //如果只有蛇头空节点，则直接返回
    if (head == NULL) {
        return;
    }

    //改变蛇的方向(只有第一个节点含有蛇的方向)
//    printf("old direction: %d\r\n", head->direction);
//    printf("new direction: %d\r\n", direction);
    head->direction = direction;
//    printf("now direction: %d\r\n", head->direction);
}


/**
 * 清空蛇,回收内存
 * @param head
 */
void free_snake(SNAKE_NODE *head) {

    SNAKE_NODE *pt = NULL;

    while (head != NULL) {
        pt = head;
        head = head->next;
        myfree(SRAMIN, pt);
    }
}

/**
 * 获取蛇的格式化信息
 * @param head 需要数据化的蛇头节点
 * @param content 传出数据内容
 * @param len 传出数据长度
 */
void getSnakeData(SNAKE_NODE *head, char **content, unsigned int *len) {
    int size, i, nodes;
    int offset = 0;
    SNAKE_NODE *temp = head;
    char *data;
    //获取蛇的节点数
    for (i = 0; temp != NULL; temp = temp->next) {
        ++i;
    }
    nodes = i;
    //计算数据总长度
    size = i * SNAKE_NODE_SIZE;//蛇节点字节数（7）
    //分配空间
    data = (char *) mymalloc(SRAMIN, size);
    //重新指向头指针
    temp = head;
    for (i = 0; i < nodes; ++i) {
        memcpy(data + offset, temp, SNAKE_NODE_SIZE);
        offset += SNAKE_NODE_SIZE;
        temp = temp->next;
    }
    //返回值赋值
    *content = data;
    *len = size;
}
