
#include "malloc.h"
#include <string.h>

#include "context.h"
#include "single_snake_service.h"
#include "snakes_cluster_service.h"
#include "food_service.h"


/**
 * 解析蛇群信息，存储蛇群,
 * 并设置 initFlag = 0x01 。
 *
 * @param data 数据源
 * @param length 数据长度
 */
void init_snakes(unsigned char *data, int length) {
    unsigned int offset = 0;
    unsigned char id = 0;
    SNAKE_NODE *nodePoint;
//    printf("init_snakes data: %d\r\n", length);
//    for (i = 0; i < length; i++) {
//        if (i % 4 == 0) printf("\r\n");
//        printf("%02x  ", data[i]);
//    }
//    printf("init_snakes data End\r\n\r\n");

    while (offset < length) {
        // 申请内存区域
//        printf("mymalloc AAA\r\n");
        nodePoint = (SNAKE_NODE *) mymalloc(SRAMIN, SNAKE_NODE_LEN);
//        printf("mymalloc BBB\r\n");
        if (nodePoint == NULL) {
            printf("init_snakes malloc error!\r\n");
            pt_mem_size();
            return;
        }
//        printf("memcpy nodePoint == NULL? %d\r\n", nodePoint == NULL);
        // 拷贝到分配的空间中
        memcpy(nodePoint, data + offset, SNAKE_NODE_SIZE);

//        printf("id: %d  (%d,%d)\r\n", nodePoint->id, nodePoint->x, nodePoint->y);

        // 增加偏移量
        offset += SNAKE_NODE_SIZE;

        // 获取玩家ID
        id = nodePoint->id;
        // 往对应的玩家ID位置加入新的节点
//        printf("add_snake_node Start\r\n");
        snakes[id] = add_snake_node(snakes[id], nodePoint);
//        printf("add_snake_node End\r\n");

    }
}

/**
 * 清空蛇群信息
 */
void clean_snakes(void) {
    unsigned char i = 0;
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        if (snakes[i] != NULL) {
            // 释放单条蛇
            free_snake(snakes[i]);
            snakes[i] = NULL;
        }
    }
}


/**
 * 更新蛇群信息，调用 move_snake ,更新每条蛇信息
 */
void refresh_snakes(void) {
    unsigned char i = 0;
    if (initFlag == 0x00) {
        return;
    }
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        if (snakes[i] != NULL) {
            move_snake(snakes[i]);
        }
    }
}

/**
 * 检查蛇群是否有吃掉食物,吃掉食物则对相应蛇增加长度，
 * 并调用 player_eat_food 消除地图上的食物
 */
void eat_grow(void) {
    unsigned char i = 0;
    if (initFlag == 0x00) {
        return;
    }
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        if (snakes[i] == NULL) continue;
        // 是否吃掉了食物
        if (player_eat_food(i, snakes[i]->x, snakes[i]->y)) {
            // 增加蛇长度
//            printf("Eat food %d, %d", snakes[i]->x, snakes[i]->y);
            grow_snake(snakes[i]);
        }
    }
}

/**
 * 获取玩家蛇头指针
 * @return 本玩家的蛇头指针
 */
SNAKE_NODE *get_player_snake(void) {
    if (initFlag == 0x00) {
        return NULL;
    }
    return snakes[PLAYER_ID];
}

/**
 * 检查每条蛇是否死亡：
 * i.  判断是否撞墙,返还 -1
 * ii. 判断是否触碰到其他蛇身体死亡时，则置空蛇信息，再判断是否是本玩家:
 *      是 - 返回杀了本玩家的玩家ID；
 *      不是 - 返回: 0x00
 * @return 死亡类型
 */
short judge_dead(void) {
    unsigned char i = 0, j = 0;
    if (initFlag == 0x00) {
        return 0;
    }

    // 检测撞墙
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        if (snakes[i] == NULL) continue;
        // 判断不撞墙
        if (snakes[i]->x >= 0 && snakes[i]->x <= MAP_WIDTH
            &&
            snakes[i]->y >= 0 && snakes[i]->y <= MAP_HEIGHT) {
            continue;
        }
        // 如果本玩家死了，那么直接返还
        if (snakes[i]->id == PLAYER_ID) {
            //free_snake(snakes[i]);
            //snakes[i] = NULL;
            return -1;
        } else {
            // 不是本玩家则释放,死掉其他蛇的空间
            free_snake(snakes[i]);
            snakes[i] = NULL;
        }
    }
    // n*n 相互检测
    for (i = 1; i <= MAX_PLAYER_NUMBER; ++i) {
        // 遍历每个蛇头
        if (snakes[i] == NULL) continue;
        for (j = 1; j <= MAX_PLAYER_NUMBER; ++j) {
            if (i == j || snakes[j] == NULL) continue;
            // 与其他蛇做比较，检查是否存在于某个蛇的身体内部
            if (checkIsIn(snakes[j], *snakes[i])) {
                // 如果本玩家死了，那么直接返还
                if (snakes[i]->id == PLAYER_ID) {
                    // 返还杀了自己的玩家的ID
                    return snakes[j]->id;
                } else {
                    // 不是本玩家则释放,死掉其他蛇的空间
                    free_snake(snakes[i]);
                    snakes[i] = NULL;
                    break;
                }
            }
        }
    }
    return 0;
}
