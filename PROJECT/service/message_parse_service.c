#include "message_parse_service.h"
#include "snakes_cluster_service.h"
#include "food_service.h"

#include "context.h"
#include <string.h>
#include "malloc.h"

/**
 * 更新游戏上下文
 * 消息类型: NO_ID_REQ/UPDATE_REQ
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @return 固定值 -1
 */
short update_context(u8 *msg, unsigned int len) {
    unsigned int offset = 0;
    unsigned char resType;
    unsigned char msgCnt;
    unsigned char id;
    unsigned char score;
    unsigned int playersInfoLen;
    unsigned int foodsLen;
    //解析类型
    memcpy(&resType, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;
//    printf("resType: %02x\r\n", resType);

    //解析次数
    memcpy(&msgCnt, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;
//    printf("msgCnt: %02x\r\n", msgCnt);

    //解析ID
    memcpy(&id, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;
//    printf("id: %02x\r\n", id);

    //解析分数
    memcpy(&score, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;
//    printf("score: %02x\r\n", score);

    //解析蛇信息长度
    memcpy(&playersInfoLen, msg + offset, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;
//    printf("playersInfoLen: %d\r\n", playersInfoLen);

    //创建蛇群
    init_snakes(msg + offset, playersInfoLen);
    //增加蛇信息长度的偏移量
    offset += playersInfoLen;
//    printf("init_snakes: success!\r\n");


    //解析食物信息长度
    memcpy(&foodsLen, msg + offset, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;
//    printf("foodsLen: %d\r\n", foodsLen);

    //创建并填充食物链表
    set_foods(msg + offset, foodsLen);
    //增加食物信息长度的偏移量
    offset += foodsLen;
//    printf("set_foods: success!\r\n");

    //判断是不是NO_ID,是的话赋值给play_id
    if (resType == NO_ID_REQ) {
        PLAYER_ID = id;
        //初始化
        initFlag = 0x01;
    }
    //myfree(SRAMIN, msg);
    // pt_mem_size();

    return -1;
}

/**
 * 结束游戏上下文
 * 消息类型： DEAD_REQ
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @return 玩家分数
 */
short end_context(u8 *msg, unsigned int len) {
    unsigned char resType;
    unsigned char msgCnt = 0;
    unsigned char id;//玩家ID
    unsigned char score = 0;//玩家分数

    // 移除请求类型数据部分
    unsigned int offset = 0;

    //1. 清除蛇群信息
    clean_snakes();
    //2. 清除地图食物信息和被本玩家吃掉的食物信息
    free_food();
    //3. 解析玩家玩家分数，并返回玩家分数

    memcpy(&resType, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    memcpy(&msgCnt, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    memcpy(&id, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    memcpy(&score, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    return score;
}

/**
 * 解析错误消息类型
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @param errorMsg 消息内容首地址(长度为总长度-2)
 * @param errorMsgLen 异常消息内容长度
 * @return 固定值 -2
 */
short error_context(u8 *msg, unsigned int len, u8 **errorMsg, int *errorMsgLen) {
    unsigned char resType;
    unsigned char msgCnt;
    unsigned int offset = 0;

    //解析类型
    memcpy(&resType, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //解析次数
    memcpy(&msgCnt, msg + offset, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    *errorMsgLen = len - 2;
    *errorMsg = (u8 *) mymalloc(SRAMIN, *errorMsgLen);
    memcpy(*errorMsg, msg + offset, *errorMsgLen);
//    myfree(SRAMIN, msg);
    return -2;
}

