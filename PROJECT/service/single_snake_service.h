#ifndef __SINGLE_SNAKE_SERVICE_H
#define __SNAKENODE_H	 

#include "context.h"

/**
 * 增长蛇
 * @param head 蛇的头部
 * @return 备增长的蛇头
 */
SNAKE_NODE * grow_snake(SNAKE_NODE *head);


/**
 * 增加蛇的节点，如果head为NULL则创建一个新的节点的头指针
 * @param head 头结点
 * @param node 需要插入的节点
 * @return 头结点
 */
SNAKE_NODE * add_snake_node(SNAKE_NODE *head, SNAKE_NODE *node);

/**
 * 检查某个结点是否存在于某条蛇内部
 * @param head 蛇头指针
 * @param node 检查节点
 * @return 在/不在 （1/0）
 */
unsigned char checkIsIn(SNAKE_NODE *head, SNAKE_NODE node);

/**
 * 移动蛇
 * 蛇头按照方向移动，身体则移动到前驱
 * @param head
 */
void move_snake(SNAKE_NODE *head);

/**
 * 改变蛇的方向
 * @param head
 * @param direction
 */
void change_snake_direction(SNAKE_NODE *head, unsigned char direction);

/**
 * 清空蛇,回收内存
 * @param head
 */
void free_snake(SNAKE_NODE *head);

/**
 * 获取蛇的格式化信息
 * @param head 需要数据化的蛇头节点
 * @param content 传出数据内容
 * @param len 传出数据长度
 */
void getSnakeData(SNAKE_NODE *head,char **content, unsigned int *len);

#endif

