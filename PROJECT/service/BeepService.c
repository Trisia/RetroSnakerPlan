#include "BeepService.h"
#include "beep.h"
#include "delay.h"


unsigned char BEEP_currentRhythm;
unsigned char BEEP_current = 0;

char rhythm1[] =
        {
                1, 1, 1, 1,
                2, 1, 2, 1,
                2, 2, 2, 1,
                4, 1, 4, 2,
                2, 2, 4, 1,
                4, 4, 4, 1,
                4, 4, 4, 1,
                4, 4, 2, 1,
                1, 1, 2, 1,
                2, 2, 2, 1,
                4, 1, 4, 1,
                4, 4, 1, 1, 0};

/**
 * 响一次蜂鸣器
 * @param duration 持续时间 毫秒
 */
void doo(unsigned short int duration) {
    BEEP = 1;
    delay_ms(duration);
    BEEP = 0;
}

/**
 * 按照给的乐谱演奏
 * @param 	rhythm 节奏
 * @param interval 每个音节持续时间
 */
void play(char *rhythm, unsigned short int interval) {
    unsigned char i = 0;
    while (1) {
        if (rhythm[i] == 0) {
            delay_ms(interval);
        } else if (rhythm[i] == 1) {
            doo(interval);
        } else {
            break;
        }
        i++;
    }
}


/**
 * 中断式演奏
 * @return 最后一个节拍 1，否则0
 */
unsigned char itCombo(void) {
    BEEP_current++;
    switch (BEEP_currentRhythm) {
        case 1: {
            if (BEEP_current <= 4)BEEP = 1;
            else BEEP = 0;
            break;
        }
        case 2: {
            if (BEEP_current <= 2 || (BEEP_current >= 4 && BEEP_current <= 6))BEEP = 1;
            else BEEP = 0;
            break;
        }
        case 4: {
            if (BEEP_current % 2 == 1)BEEP = 1;
            else BEEP = 0;
            break;
        }
        default:
            break;
    }
    if (BEEP_current == 8) return 1;
    return 0;
}

/**
 * 设置4分音符节拍
 * @param rhythm 节拍 1 - 4
 */
void setCurrent(char rhythm) {
    BEEP_currentRhythm = rhythm;
    BEEP_current = 0;
}

/**
 * 中断式的演奏
 * @param rhythm 音符表，4（1）,8（2）,16（4）分音符
 * @param loop 是否循播放
 */
void itPlayCombo(char *rhythm, unsigned char loop) {
    static unsigned char currentRhythmIndex = 0;
    static unsigned char firstTimePlayer = 1;
    unsigned char isEnd = 0;
    // 第一次播放得先设置音符
    if (firstTimePlayer) {
        setCurrent(rhythm[currentRhythmIndex]);
        firstTimePlayer = 0;
    }
    // 遇到结束符0结束播放
    if (rhythm[currentRhythmIndex] == 0) {
        if (loop) {
            currentRhythmIndex = 0; // 循环播放
            setCurrent(rhythm[currentRhythmIndex]);
        } else return;
    }
    // 播放并且检查是否是最后一个节拍
    isEnd = itCombo();
    // 最后一个节拍
    if (isEnd) {
        // 切换下一个音符
        currentRhythmIndex++;
        setCurrent(rhythm[currentRhythmIndex]);
    }
}

/**
 * 按照给的连打演奏
 * @param 	rhythm 节奏
 * @param interval 每个音节持续时间
 */
void playCombo(char *rhythm, unsigned short int interval) {
    unsigned char i = 0;
    while (1) {
        if (rhythm[i] == 0) {
            break;
        } else {
            combo(rhythm[i], interval);
        }
        i++;
    }
}


/**
 * 连打
 * @param part 连打排数
 * @param duration 持续时间
 */
void combo(char part, unsigned short int duration) {
    unsigned short int interval = duration / (part * 2);
    unsigned char i = 0;
    for (i = 0; i < (part * 2); ++i) {
        if (i % 2 == 0) {
            doo(interval);
        } else {
            delay_ms(interval);
        }
    }
}


/**
 * 声音1
 */
void voice1(void) {

//    char rhythm[] = {1, 1, 1, 1,0};
//    char rhythm[] = {
//            1, 1, 0, 1, 0, 0, 0, 0, 9
//    };
//    char rhythm[] = {0,0,	0,0,	0,0,		1,1,
//                     1,1,	0,0,	1,0,1,0,	1,1,
//                     1,1,	0,0,	1,0,1,0,	1,1,
//                     0,0,	1,1,	0,0,		1,0,1,0,
//                     1,1,	0,0,	0,0,		0,0, 9};
//    char rhythm[] = {1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1};
//    play(rhythm, 70);

    playCombo(rhythm1, 500);
}


