#ifndef RETROSNAKERPLAN_BEEPSERVICE_H
#define RETROSNAKERPLAN_BEEPSERVICE_H

extern unsigned char BEEP_currentRhythm;
extern unsigned char BEEP_current;
extern char rhythm1[];

/**
 * 设置4分音符节拍
 * @param rhythm 节拍 1 - 4
 */
void setCurrent(char rhythm);

/**
 * 中断式演奏
 * @return 最后一个节拍 1，否则0
 */
unsigned char itCombo(void);

/**
 * 中断式的演奏
 * @param rhythm 音符表，4（1）,8（2）,16（4）分音符
 * @param loop 是否循播放
 */
void itPlayCombo(char *rhythm, unsigned char loop);


/**
 * 声音1
 */
void voice1(void);

/**
 * 响一次蜂鸣器
 * @param duration 持续时间 毫秒
 */
void doo(unsigned short int duration);

/**
 * 连打
 * @param part 连打排数
 * @param duration 持续时间
 */
void combo(char part, unsigned short int duration);

/**
 * 按照给的连打演奏
 * @param 	rhythm 节奏
 * @param interval 每个音节持续时间
 */
void playCombo(char *rhythm, unsigned short int interval);

/**
 * 按照给的乐谱演奏
 * @param 	rhythm 节奏
 * @param interval 每个音节持续时间
 */
void play(char *rhythm, unsigned short int interval);

#endif //RETROSNAKERPLAN_BEEPSERVICE_H
