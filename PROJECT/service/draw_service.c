#include "draw_service.h"


/**
 * 根据传入坐标，初始化座标转换参数
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 */
void set_draw_context(unsigned short int x, unsigned short int y) {
    short visible_offset_x = lcddev.width / MAP_SCALE / 2;
    short visible_offset_y = lcddev.height / MAP_SCALE / 2;
    DRAW_OFFSET_SX = x - visible_offset_x;
    DRAW_OFFSET_SY = y - visible_offset_y;
    DRAW_OFFSET_EX = DRAW_OFFSET_SX + (lcddev.width / MAP_SCALE);
    DRAW_OFFSET_EY = DRAW_OFFSET_SY + (lcddev.height / MAP_SCALE);
}

/**
 * 在地图上绘制点，该步骤将会转换坐标为实际屏幕坐标
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 * @param colorType 颜色类型
 */
void draw_point(unsigned short int x, unsigned short int y, unsigned char colorType) {
    unsigned short int colorValue = chooseColor(colorType);
    unsigned short int sx, sy, ex, ey;
    sx = (x - DRAW_OFFSET_SX) * MAP_SCALE;
    ex = sx + MAP_SCALE;

    sy = (y - DRAW_OFFSET_SY) * MAP_SCALE;
    ey = sy + MAP_SCALE;
    // 填充
    LCD_Fill(sx, sy, ex, ey, colorValue);
}

/**
 * 绘制蛇节点
 * @param drawNode 绘制的蛇节点
 */
void draw_node(SNAKE_NODE drawNode) {
    unsigned short int x = drawNode.x;
    unsigned short int y = drawNode.y;
    unsigned char colorType = drawNode.colorType;
    // 首先绘制基础颜色
    draw_point(x, y, colorType);
    // 绘制黑色外边框
    draw_box(x, y, 1);
    // 绘制条纹,颜色为当前颜色的下一个颜色值
    draw_stripe(drawNode);
}

/**
 * 绘制T形状的斑纹
 * @param drawNode 绘制的蛇节点
 */
void draw_stripe(SNAKE_NODE drawNode) {

    unsigned short int x = drawNode.x;
    unsigned short int y = drawNode.y;

    unsigned char colorType = (drawNode.colorType + 1) % 20;
    unsigned char direction = drawNode.direction;


    unsigned short int colorValue = chooseColor(colorType);
    unsigned short int sx, sy, ex, ey;
    // unsigned short int width, height;

    sx = (x - DRAW_OFFSET_SX) * MAP_SCALE;
    ex = sx + MAP_SCALE;

    sy = (y - DRAW_OFFSET_SY) * MAP_SCALE;
    ey = sy + MAP_SCALE;

    switch (direction) {
        case DIRECTION_UP:
            LCD_Fill(sx, sy, ex, sy + MAP_SCALE / 5, colorValue);
            break;
        case DIRECTION_DOWN:
            LCD_Fill(sx, sy + (MAP_SCALE * 4 / 5), ex, ey, colorValue);
            break;
        case DIRECTION_LEFT:
            LCD_Fill(sx, sy, sx + (MAP_SCALE / 5), ey, colorValue);
            break;
        case DIRECTION_RIGHT:
            LCD_Fill(sx + (MAP_SCALE * 4 / 5), sy, ex, ey, colorValue);
            break;
    }


//    // 填充
//    LCD_DrawRectangle(sx, sy, ex, ey, colorValue);
}

/**
 * 绘制空心矩形
 * @param x 虚拟地图X坐标
 * @param y 虚拟地图Y坐标
 * @param colorType 颜色类型
 */
void draw_box(unsigned short int x, unsigned short int y, unsigned char colorType) {
    unsigned short int colorValue = chooseColor(colorType);
    unsigned short int sx, sy, ex, ey;
    sx = (x - DRAW_OFFSET_SX) * MAP_SCALE;
    ex = sx + MAP_SCALE;

    sy = (y - DRAW_OFFSET_SY) * MAP_SCALE;
    ey = sy + MAP_SCALE;
    // 填充
    POINT_COLOR = colorValue;
    LCD_DrawRectangle(sx, sy, ex, ey);
}


/**
 * 根据颜色类型获取16bit的颜色值 5/6/5
 * @param type 颜色类型
 * @return 565颜色值
 */
unsigned short int chooseColor(unsigned char type) {
    unsigned short int res = BLACK;
    switch (type) {
        case 0:
            res = WHITE;
            break;
        case 1:
            res = BLACK;
            break;
        case 2:
            res = BLUE;
            break;
        case 3:
            res = BRED;
            break;
        case 4:
            res = GRED;
            break;
        case 5:
            res = GBLUE;
            break;
        case 6:
            res = RED;
            break;
        case 7:
            res = MAGENTA;
            break;
        case 8:
            res = GREEN;
            break;
        case 9:
            res = CYAN;
            break;
        case 10:
            res = YELLOW;
            break;
        case 11:
            res = BROWN;
            break;
        case 12:
            res = BRRED;
            break;
        case 13:
            res = GRAY;
            break;
        case 14:
            res = DARKBLUE;
            break;
        case 15:
            res = LIGHTBLUE;
            break;
        case 16:
            res = GRAYBLUE;
            break;
        case 17:
            res = LIGHTGREEN;
            break;
        case 18:
            res = LGRAY;
            break;
        case 19:
            res = LGRAYBLUE;
            break;
        case 20:
            res = LBBLUE;
            break;
    }
    return res;
}

/**
 * 绘制背景
 */
void draw_background(void) {
    short width = lcddev.width / MAP_SCALE;
    short height = lcddev.height / MAP_SCALE;
    //LCD_Clear(WALL_COLOR);
    unsigned short int sx, sy, ex, ey;
    sx = 0;
    sy = 0;
    ex = lcddev.width;
    ey = lcddev.height;

    // 在地图区域外部
    if (!(DRAW_OFFSET_SX >= 0 &&
          DRAW_OFFSET_SY >= 0 &&
          DRAW_OFFSET_EX <= MAP_WIDTH &&
          DRAW_OFFSET_EY <= MAP_HEIGHT)) {
        // 有区域超过地图区域
        // 先填充绘制墙
        LCD_Clear(WALL_COLOR);
        // X坐标
        if (DRAW_OFFSET_SX < 0) {
            sx = DRAW_OFFSET_SX * -1 * MAP_SCALE;
        }
        if (DRAW_OFFSET_EX > MAP_WIDTH) {
            ex = (width - (DRAW_OFFSET_EX - MAP_WIDTH)) * MAP_SCALE;
        }
        // Y坐标
        if (DRAW_OFFSET_SY < 0) {
            sy = DRAW_OFFSET_SY * -1 * MAP_SCALE;
        }
        if (DRAW_OFFSET_EY > MAP_HEIGHT) {
            ey = (height - (DRAW_OFFSET_EY - MAP_HEIGHT)) * MAP_SCALE;
        }
    }
    // 绘制背景
    LCD_Fill(sx, sy, ex, ey, BACKGROUND_COLOR);
}
