#include "network_service.h"
#include "single_snake_service.h"
#include "snakes_cluster_service.h"
#include "food_service.h"
#include "wifiSta.h"
#include "context.h"
#include "malloc.h"
#include <string.h>

/**
 * 发送开启游戏请求
 */
void start_game(void) {
    unsigned char type = NO_ID_REQ;
    u8 content[UNSIGNED_CHAR_LEN];
    memcpy(content, &type, UNSIGNED_CHAR_LEN);
//    printf("request: %d\r\n",content);
    sendUDPMsg(content, UNSIGNED_CHAR_LEN);
}

/**
 * 发送更新游戏请求，必须先初始化地图信息
 */
void update_game(void) {
    //请求类型
    unsigned char type = UPDATE_REQ;

    int len;//消息长度
    u8 *content;//消息内容
    unsigned int offset = 0;//偏移量
    //与蛇信息相关的变量
    SNAKE_NODE *head;//玩家蛇头指针

    unsigned int nodes_len;//玩家蛇信息长度
    char *snake_data;//蛇数据内容

    //与食物相关变量
    unsigned int food_nodes_len;
    char *ateFoods_date;


    // 如果没有初始化则直接返还
    if (initFlag == 0x00) {
        return;
    }

    //获取玩家蛇信息数据 和 长度
    //本玩家的蛇头指针
    head = get_player_snake();
    //获取信息
    getSnakeData(head, &snake_data, &nodes_len);
//    printf("nodes_len: %d\r\n", nodes_len);
    //获取食物信息

    printf("\r\n\r\nmsgCnt: %d\r\n", msgCnt);
    getAteFoodsData(&ateFoods_date, &food_nodes_len);
//    printf("food_nodes_len: %d\r\n", food_nodes_len);

    //计算消息长度
    len = UNSIGNED_CHAR_LEN * 3 + UNSIGNED_INT_LEN + nodes_len + UNSIGNED_INT_LEN + food_nodes_len;
    content = (u8 *) mymalloc(SRAMIN,len);
//    printf("message len: %d\r\n", len);

    //组装发送信息

    //1 type 固定值1(UPDATE)
    memcpy(content + offset, &type, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //2 msgCnt 消息计数器
    //判断消息计数器是否达到上限,达到上限则置零
    if (msgCnt >= MAX_REQUEST_TIMES) {
        msgCnt = 0;
    }



    memcpy(content + offset, &msgCnt, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //3 id 玩家ID
    memcpy(content + offset, &PLAYER_ID, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //4 playerInfoLen
    memcpy(content + offset, &nodes_len, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;

    //5 playerInfo
    memcpy(content + offset, snake_data, nodes_len);
    offset += nodes_len;

    //6 eatsLen
    memcpy(content + offset, &food_nodes_len, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;

    //7 eats
    memcpy(content + offset, ateFoods_date, food_nodes_len);
    offset += food_nodes_len;

    //发送消息
    sendUDPMsg(content, len);

    //释放空间
    myfree(SRAMIN,ateFoods_date);
    myfree(SRAMIN,snake_data);
    myfree(SRAMIN,content);
}

/**
 * 发送游戏玩家死亡请求，必须先初始化地图信息
 */
void dead_game(unsigned char killerId) {

    //请求类型
    unsigned char type = DEAD_REQ;

    int len;//消息长度
    u8 *content;//消息内容
    unsigned int offset = 0;//偏移量
    //与蛇信息相关的变量
    SNAKE_NODE *head;//玩家蛇头指针
    unsigned int nodes_len;//玩家蛇信息长度
    char *snake_data;//蛇数据内容
    //与食物相关变量
    char *ateFoods_date;
    unsigned int food_nodes_len;

    // 如果没有初始化则直接返还
    if (initFlag == 0x00) {
        return;
    }


    //获取玩家蛇信息数据 和 长度
    //本玩家的蛇头指针
    head = get_player_snake();
    //获取信息
    getSnakeData(head, &snake_data, &nodes_len);

    //获取食物信息
    getAteFoodsData(&ateFoods_date, &food_nodes_len);


    //计算消息长度
    len = UNSIGNED_CHAR_LEN * 4 + UNSIGNED_INT_LEN + nodes_len + UNSIGNED_INT_LEN + food_nodes_len;
    content = (u8 *) mymalloc(SRAMIN,len);

    //组装发送信息

    //1 type 固定值2(DEAD)
    memcpy(content + offset, &type, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //2 msgCnt 消息计数器
    //判断消息计数器是否达到上限,达到上限则置零
    if (msgCnt >= MAX_REQUEST_TIMES) {
        msgCnt = 0;
    }
    memcpy(content + offset, &msgCnt, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //3 id 玩家ID
    memcpy(content + offset, &PLAYER_ID, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //4 killer 击杀者ID
    memcpy(content + offset, &killerId, UNSIGNED_CHAR_LEN);
    offset += UNSIGNED_CHAR_LEN;

    //4 playerInfoLen
    memcpy(content + offset, &nodes_len, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;

    //5 playerInfo
    memcpy(content + offset, snake_data, nodes_len);
    offset += nodes_len;

    //6 eatsLen
    memcpy(content + offset, &food_nodes_len, UNSIGNED_INT_LEN);
    offset += UNSIGNED_INT_LEN;

    //7 eats
    memcpy(content + offset, ateFoods_date, food_nodes_len);
    offset += food_nodes_len;

    //发送消息
    sendUDPMsg(content, len);

    //释放空间
    myfree(SRAMIN,content);
    myfree(SRAMIN,snake_data);
    myfree(SRAMIN,ateFoods_date);
}

