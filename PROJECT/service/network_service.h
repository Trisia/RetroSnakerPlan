#ifndef __NETWORK_SERVICE__
#define __NETWORK_SERVICE__

#include "context.h"

/**
 * 发送开启游戏请求
 */
void start_game(void);

/**
 * 发送更新游戏请求，必须先初始化地图信息
 */
void update_game(void);

/**
 * 发送游戏玩家死亡请求，必须先初始化地图信息
 */
void dead_game(unsigned char killerId);

#endif //RETROSNAKERPLAN_NETWORK_SERVICE_H
