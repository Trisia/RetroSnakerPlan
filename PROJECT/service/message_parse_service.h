#ifndef _MESSAGE_PARSE_SERVICE_H
#define _MESSAGE_PARSE_SERVICE_H

#include "sys.h" 

/**
 * 更新游戏上下文
 * 消息类型: NO_ID_REQ/UPDATE_REQ
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @return 固定值 -1
 */
short update_context(u8 *msg,unsigned int len);

/**
 * 结束游戏上下文
 * 消息类型： DEAD_REQ
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @return 玩家分数
 */
short end_context(u8 *msg,unsigned int len);

/**
 * 解析错误消息类型
 * @param msg 消息内容首地址
 * @param len 消息长度
 * @param errorMsg 异常消息内容首地址
 * @param errorMsgLen 异常消息内容长度
 * @return 固定值 -2
 */
short error_context(u8 *msg, unsigned int len, u8 **errorMsg,int *errorMsgLen);

#endif

