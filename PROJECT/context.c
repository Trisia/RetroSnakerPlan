#include "context.h"
#include "malloc.h"
#include "usart.h"

//蛇群数组
SNAKE_NODE *snakes[51];
//初始化标志
char initFlag;
unsigned char PLAYER_ID;

//食物相关
FOOD_NODE *foods;//地图上维护的食物链表
FOOD_NODE *ateFoods;//吃掉的食物链表

//绘制相关变量
short int DRAW_OFFSET_SX;
short int DRAW_OFFSET_SY;
short int DRAW_OFFSET_EX;
short int DRAW_OFFSET_EY;

unsigned char beforeDirection;

unsigned char msgCnt = 0x00;
unsigned char playBGM = 1;

/**
 * 内存测量
 */
void pt_mem_size(void) {
    int size = 1;
    static int maxSize = 30000;
    char *ppp;

    size = maxSize;
    ppp = (char *) mymalloc(SRAMIN, sizeof(char) * size);
    if (ppp != NULL) {
        do {
            myfree(SRAMIN, ppp);
            size++;
            ppp = (char *) mymalloc(SRAMIN, sizeof(char) * size);
        } while (ppp != NULL);
    } else {
        do {
            size--;
            if (size == 0)break;
            myfree(SRAMIN, ppp);
            ppp = (char *) mymalloc(SRAMIN, sizeof(char) * size);
        } while (ppp == NULL);
        myfree(SRAMIN, ppp);
    }
    maxSize = size;
    printf("Remaining space: %d byte \r\n\r\n", size);
}
