#include "draw_controller.h"
#include "context.h"
#include "draw_service.h"
#include "snakes_cluster_service.h"
#include "lcd.h"
#include "hanzi.h"
#include "delay.h"
#include "usart.h"
#include "remote.h"
#include "touch.h"

/**
 * 绘制画面，必须先初始化数据
 */
void reader_screen(void) {
    FOOD_NODE *fp;
    SNAKE_NODE *snp;
    char i;
    short visible_sx, visible_ex, visible_sy, visible_ey;

    if (initFlag == 0x00) {
        return;
    }
    // 获取本玩家的蛇的头指针
    snp = get_player_snake();
    // 设置可视区域
    visible_sx = snp->x - (lcddev.width / MAP_SCALE / 2);
    visible_ex = snp->x + (lcddev.width / MAP_SCALE / 2);
    visible_sy = snp->y - (lcddev.height / MAP_SCALE / 2);
    visible_ey = snp->y + (lcddev.height / MAP_SCALE / 2);
//    printf("visible x [%d, %d]\r\n", visible_sx, visible_ex);
//    printf("visible y [%d, %d]\r\n", visible_sy, visible_ey);
    // 使用头结点初始化地图信息
    set_draw_context(snp->x, snp->y);
    // 绘制背景
    draw_background();

    // 遍历每一个食物节点，然后绘制
    fp = foods;
    while (fp != NULL) {
        // 绘制点
        if ((fp->x >= visible_sx) && (fp->x <= visible_ex)
            &&
            (fp->y >= visible_sy) && (fp->y <= visible_ey)) {
            // 在可视区域内部，绘制点
//            printf("visible food x: %d, y: %d\r\n", fp->x, fp->y);
            draw_point(fp->x, fp->y, fp->colorType);
        }
//        else {
//            printf("invisible food x: %d, y: %d\r\n", fp->x, fp->y);
//        }

        fp = fp->next;
    }
    // 遍历每一个蛇的节点
    for (i = 1; i <= MAX_PLAYER_NUMBER; i++) {
        if (snakes[i] == NULL)continue;
        snp = snakes[i];
        // 遍历每蛇的每一个节点
        while (snp != NULL) {
            // 判断是否在可视区域内部
            if ((snp->x >= visible_sx && snp->x <= visible_ex)
                &&
                (snp->y >= visible_sy && snp->y <= visible_ey)) {
                // 在可视区域内部，绘制点
//                draw_point(snp->x, snp->y, snp->colorType);
                draw_node(*snp);
            }
            snp = snp->next;
        }
    }
}


/**
 * 绘制欢迎页
 */
void draw_welcome_page(char type,short score) {
    char key;
    short temp_x, temp_y, pre_x, pre_y;

    SNAKE_NODE node1, node2, node3, node4, node5;
    LCD_Clear(WHITE);//清屏
    //初始化数据
    node1.x = 128;
    node1.y = 0;
    node1.direction = 2;
    node2.x = 144;
    node2.y = 0;
    node2.direction = 2;
    node3.x = 160;
    node3.y = 0;
    node3.direction = 2;
    node4.x = 176;
    node4.y = 0;
    node4.colorType = 3;
    node4.direction = 2;
    node5.x = 192;
    node5.y = 0;
    node5.direction = 2;
    node1.next = &node2;
    node2.next = &node3;
    node3.next = &node4;
    node4.next = &node5;
    pre_x = 208;
    pre_y = 224;
    //节点下落
    while (1) {
        if (node1.y != 224) {
            node1.y++;
            LCD_Fill(node1.x, node1.y, node1.x + 16, node1.y + 16, BLUE);
            LCD_Fill(node1.x, node1.y - 1, node1.x + 16, node1.y, WHITE);
        }
        if (node1.y >= 20 && node2.y != 224) {
            node2.y++;
            LCD_Fill(node2.x, node2.y, node2.x + 16, node2.y + 16, BRED);
            LCD_Fill(node2.x, node2.y - 1, node2.x + 16, node2.y, WHITE);
        }
        if (node2.y >= 20 && node3.y != 224) {
            node3.y++;
            LCD_Fill(node3.x, node3.y, node3.x + 16, node3.y + 16, GRED);
            LCD_Fill(node3.x, node3.y - 1, node3.x + 16, node3.y, WHITE);
        }
        if (node3.y >= 20 && node4.y != 224) {
            node4.y++;
            LCD_Fill(node4.x, node4.y, node4.x + 16, node4.y + 16, GBLUE);
            LCD_Fill(node4.x, node4.y - 1, node4.x + 16, node4.y, WHITE);
        }
        if (node4.y >= 20 && node5.y != 224) {
            node5.y++;
            LCD_Fill(node5.x, node5.y, node5.x + 16, node5.y + 16, MAGENTA);
            LCD_Fill(node5.x, node5.y - 1, node5.x + 16, node5.y, WHITE);
            if (node5.y == 224) {
                break;
            }
        }
        delay_us(3000);
    }

    if (type == DRAW_PAGE_TYPE_START){
        //绘制字体
        fWriteHz24(GAME_TITLE[0].Msk, 96, 60, 1, RED);
        fWriteHz24(GAME_TITLE[1].Msk, 120, 60, 1, RED);
        fWriteHz24(GAME_TITLE[2].Msk, 144, 60, 1, RED);
        fWriteHz24(GAME_TITLE[3].Msk, 168, 60, 1, RED);
        fWriteHz24(GAME_TITLE[4].Msk, 192, 60, 1, RED);
        fWriteHz24(GAME_TITLE[5].Msk, 216, 60, 1, RED);

        fWriteHz24(Author[0].Msk, 96, 95, 1, RED);
        fWriteHz24(Author[1].Msk, 120, 95, 1, RED);
        fWriteHz24(Author[2].Msk, 144, 95, 1, RED);
        fWriteHz24(Author[3].Msk, 168, 95, 1, RED);
        fWriteHz24(Author[4].Msk, 192, 95, 1, RED);
        fWriteHz24(Author[5].Msk, 216, 95, 1, RED);
        fWriteHz24(Author[6].Msk, 168, 130, 1, RED);
        fWriteHz24(Author[7].Msk, 192, 130, 1, RED);
        fWriteHz24(Author[8].Msk, 216, 130, 1, RED);

//        fWriteHz24(info[0].Msk, 96, 165, 1, RED);
        POINT_COLOR = RED;
//        LCD_ShowxNum(130, 165, 1, 1, 24, 0);

        LCD_DrawRectangle(118,163,218,191);
        fWriteHz24(info[1].Msk, 120, 165, 1, RED);
        fWriteHz24(info[2].Msk, 144, 165, 1, RED);
        fWriteHz24(info[3].Msk, 168, 165, 1, RED);
        fWriteHz24(info[4].Msk, 192, 165, 1, RED);

    }else {
        fWriteHz24(end_info[0].Msk, 96, 95, 1, RED);
        fWriteHz24(end_info[1].Msk, 120, 95, 1, RED);
        fWriteHz24(end_info[2].Msk, 144, 95, 1, RED);
        fWriteHz24(end_info[3].Msk, 168, 95, 1, RED);
        fWriteHz24(end_info[4].Msk, 96, 130, 1, RED);
        fWriteHz24(end_info[5].Msk, 120, 130, 1, RED);

        POINT_COLOR = RED;
        LCD_ShowxNum(150, 130, score, 2, 24, 0);




//        fWriteHz24(info[0].Msk, 96, 165, 1, RED);
        POINT_COLOR = RED;
//        LCD_ShowxNum(130, 165, 1, 1, 24, 0);
        LCD_DrawRectangle(118,163,218,191);
        fWriteHz24(info[1].Msk, 120, 165, 1, RED);
        fWriteHz24(info[2].Msk, 144, 165, 1, RED);
        fWriteHz24(info[3].Msk, 168, 165, 1, RED);
        fWriteHz24(info[4].Msk, 192, 165, 1, RED);
    }




    //蛇身移动
    while (1) {
        tp_dev.scan(0);
        if(tp_dev.sta&TP_PRES_DOWN){//触摸屏被按下
            if(tp_dev.x[0]<lcddev.width&&tp_dev.y[0]<lcddev.height){
                //屏幕范围内
                if ( tp_dev.x[0] >= 118 && tp_dev.x[0] <= 218 && tp_dev.y[0] >= 163 && tp_dev.y[0] <= 191 ){
                    break;
                }
            }
        }

        LCD_Fill(node1.x, node1.y, node1.x + 16, node1.y + 16, BLUE);
        LCD_Fill(node2.x, node2.y, node2.x + 16, node2.y + 16, BRED);
        LCD_Fill(node3.x, node3.y, node3.x + 16, node3.y + 16, GRED);
        LCD_Fill(node4.x, node4.y, node4.x + 16, node4.y + 16, GBLUE);
        LCD_Fill(node5.x, node5.y, node5.x + 16, node5.y + 16, MAGENTA);
        LCD_Fill(pre_x, pre_y, pre_x + 16, pre_y + 16, WHITE);

        //移动方向
        if (node1.x == 304 && node1.y == 0) {
            node1.direction = 1;
        }
        if (node1.x == 304 && node1.y == 224) {
            node1.direction = 2;
        }
        if (node1.x == 0 && node1.y == 224) {
            node1.direction = 0;
        }
        if (node1.x == 0 && node1.y == 0) {
            node1.direction = 3;
        }
        pre_x = node1.x;
        pre_y = node1.y;
        switch (node1.direction) {
            case DIRECTION_UP: {
                node1.y -= 16;
                break;
            }
            case DIRECTION_DOWN: {
                node1.y += 16;
                break;
            }
            case DIRECTION_LEFT: {
                node1.x -= 16;
                break;
            }
            case DIRECTION_RIGHT: {
                node1.x += 16;
                break;
            }
            default: {
                break;
            }
        }
        temp_x = node2.x;
        temp_y = node2.y;

        node2.x = pre_x;
        node2.y = pre_y;

        pre_x = temp_x;
        pre_y = temp_y;

        temp_x = node3.x;
        temp_y = node3.y;

        node3.x = pre_x;
        node3.y = pre_y;

        pre_x = temp_x;
        pre_y = temp_y;

        temp_x = node4.x;
        temp_y = node4.y;

        node4.x = pre_x;
        node4.y = pre_y;

        pre_x = temp_x;
        pre_y = temp_y;

        temp_x = node5.x;
        temp_y = node5.y;

        node5.x = pre_x;
        node5.y = pre_y;

        pre_x = temp_x;
        pre_y = temp_y;

        delay_ms(200);
        key = delay_Remote_Scan();
        if (key == BUTTON_1) {
            //按下1键开始游戏，结束循环
            break;
        }

        tp_dev.scan(0);
        if(tp_dev.sta&TP_PRES_DOWN){//触摸屏被按下
            if(tp_dev.x[0]<lcddev.width&&tp_dev.y[0]<lcddev.height){
                //屏幕范围内
                if ( tp_dev.x[0] >= 118 && tp_dev.x[0] <= 218 && tp_dev.y[0] >= 163 && tp_dev.y[0] <= 191 ){
                    break;
                }
            }
        }



    }
}
