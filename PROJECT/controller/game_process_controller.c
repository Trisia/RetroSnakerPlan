#include "remote.h"
#include "context.h"
#include "game_process_controller.h"
#include "snakes_cluster_service.h"
#include "single_snake_service.h"
#include "network_service.h"
#include "context.h"
#include "key.h"
#include "food_service.h"
#include "message_controller.h"
#include "draw_controller.h"
#include "usart.h"


/**
 * 游戏流程：
 * 监听按键事件，判断是否需要更改玩家方向 change_snake_direction()
 * 更新蛇位置信息 reflash_snakes()
 * 判断蛇群有没有吃掉食物 eat_grow() 增长吃掉食物的蛇的长度，消去地图食物，存储玩家吃掉的食物。
 * 死亡判断 judge_dead() ，并返回。
 * 死亡：杀死本玩家ID / -1 撞墙
 * 非死亡：0x00
 */
short game_process(void) {

    //更新蛇位置信息
    refresh_snakes();

    //判断蛇群有没有吃掉食物 增长吃掉食物的蛇的长度，消去地图食物，存储玩家吃掉的食物。
    eat_grow();

    //死亡判断 judge_dead()
    return judge_dead();

}


/**
 * 外部入口
 * 游戏总控制器
 * @return 游戏分数
 */
short start_game_process(void) {
    short game_res, msgType;
    short t = 999;
    //消息计数器置零
    msgCnt = 0x00;
    //发送初始化请求
    printf("send start game request...\r\n");

    //监听消息 判断是否初始化
    while (1) {
        t++;
        if (t == 1000) {
            start_game();
            t = 0;
        }
        msgType = listen_message_and_operator();
        if (initFlag == 0x01) {
            //初始化后开始游戏
            printf("init ok...\r\n");
            t = 0;
            reader_screen();
            break;
        }
    }

    //游戏流程
    while (1) {
        //增加消息计数器
        msgCnt++;
        //游戏控制器流程
        game_res = game_process();

        //是否死亡 或 更新
        if (game_res == 0x00) {
            //更新
            //发送UPDATE信息
            update_game();
            //绘制
            reader_screen();
        } else {
            //-1 撞墙
            // 或杀死本玩家的ID
            //发送死亡消息
            if (game_res == -1) game_res = 0x00;
            dead_game(game_res);
            printf("dead\r\n");
            while (1) {
                msgType = listen_message_and_operator();
                if (msgType >= 0) {
                    //死亡消息，返回分数
                    //消息计数器置零
                    msgCnt = 0x00;
                    initFlag = 0x00;
                    //清空
                    clean_snakes();
                    free_food();
                    return msgType;
                }
            }
        }

        //继续监听
        msgType = listen_message_and_operator();

//        printf("End once:\r\n");
//        pt_mem_size();
//        printf("\r\n\r\n");
    }
}
