#ifndef _GAME_PROCESS_CONTROLLER_H
#define _GAME_PROCESS_CONTROLLER_H

/**
 * 游戏流程：
 * 监听按键事件，判断是否需要更改玩家方向 change_snake_direction()
 * 更新蛇位置信息 reflash_snakes()
 * 判断蛇群有没有吃掉食物 eat_grow() 增长吃掉食物的蛇的长度，消去地图食物，存储玩家吃掉的食物。
 * 死亡判断 judge_dead() ，并返回。
 * 死亡：杀死本玩家ID / -1 撞墙
 * 非死亡：0x00
 */
short game_process(void);

/**
 * 外部入口
 * 游戏总控制器
 * @return 游戏分数
 */
short start_game_process(void);


#endif //RETROSNAKERPLAN_GAME_PROCESS_CONTROLLER_H
