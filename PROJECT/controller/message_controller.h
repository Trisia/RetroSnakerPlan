#ifndef _MESSAGE_CONTROLLER_H
#define _MESSAGE_CONTROLLER_H

/**
 * 监听服务端消息
 * 调用消息解析服务:
 *  非死亡消息，则更新游戏上下文update_context(),返回 -1
 *  死亡消息，则返回分数，范围：[0,255]
 *  无消息，返回 -1
 *  异常消息 -2
 * @return
 */
short listen_message_and_operator(void);

#endif //RETROSNAKERPLAN_MESSAGE_CONTROLLER_H
