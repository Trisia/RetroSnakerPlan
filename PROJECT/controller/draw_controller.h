#ifndef __DRAW_CONTROLLER__
#define __DRAW_CONTROLLER__

/**
 * 绘制画面，必须先初始化数据
 */
void reader_screen(void);


/**
 * 绘制欢迎界面/结束界面
 * @param type 绘制类型 (DRAW_PAGE_TYPE_START 欢迎界面) (DRAW_PAGE_TYPE_END 死亡界面)
 * @param score 当 type 为 DRAW_PAGE_TYPE_END时有效，用于绘制分数
 */
void draw_welcome_page(char type,short score);


#endif //RETROSNAKERPLAN_DRAW_CONTROLLER_H
