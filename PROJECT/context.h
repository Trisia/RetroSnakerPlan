#ifndef __CONTEXT_H
#define __CONTEXT_H

//蛇节点
typedef struct snake_node SNAKE_NODE;
struct snake_node {
    short x;           //x坐标
    short y;           //y坐标
    unsigned char colorType;   //颜色类别
    unsigned char id;          //所属玩家ID
    unsigned char direction;   //蛇运动方向
    SNAKE_NODE *next;
};
#define SNAKE_NODE_SIZE 7 // 蛇节点字节数

//食物节点
typedef struct Food_node FOOD_NODE;
struct Food_node {
    short x;
    short y;
    unsigned char colorType;
    FOOD_NODE *next;
};
#define FOOD_NODE_SIZE 5 // 食物节点字节数



//玩家总数
#define MAX_PLAYER_NUMBER 50
//玩家蛇最大长度
#define PLAYER_MAX_LEN 200
//蛇群数组
extern SNAKE_NODE *snakes[51];
//初始化标志
extern char initFlag;
extern unsigned char PLAYER_ID;

//食物相关
extern FOOD_NODE *foods;//地图上维护的食物链表
extern FOOD_NODE *ateFoods;//吃掉的食物链表

//请求相关的变量
#define NO_ID_REQ 0xFF
#define UPDATE_REQ 0x01
#define DEAD_REQ 0x02
#define ERROR_REQ 0x03

// 类型占用字节大小
#define UNSIGNED_CHAR_LEN (sizeof(unsigned char))
#define UNSIGNED_SHORT_LEN (sizeof(unsigned short))
#define UNSIGNED_INT_LEN (sizeof(unsigned int))
#define SNAKE_NODE_LEN (sizeof(SNAKE_NODE))
#define FOOD_NODE_LEN (sizeof(FOOD_NODE))

//定义方向
#define DIRECTION_UP 0x00
#define DIRECTION_DOWN 0x01
#define DIRECTION_LEFT 0x02
#define DIRECTION_RIGHT 0x03

//绘制相关变量
//地图缩放比例
#define MAP_WIDTH 100
#define MAP_HEIGHT 100
#define MAP_SCALE 16
#define BACKGROUND_COLOR 0xFFFF
#define WALL_COLOR 0x0000
extern short int DRAW_OFFSET_SX;
extern short int DRAW_OFFSET_SY;
extern short int DRAW_OFFSET_EX;
extern short int DRAW_OFFSET_EY;

//蛇头初始方向或上一次
extern unsigned char beforeDirection;

//最大网络请求次数
#define MAX_REQUEST_TIMES 100
//记录请求次数
extern unsigned char msgCnt;
extern unsigned char playBGM;

/**
 * 内存测量
 */
extern void pt_mem_size(void);

//定义按键
#define BUTTON_POWER 162
#define BUTTON_UP 98
#define BUTTON_PLAY 2
#define BUTTON_ALIENTEK 226
#define BUTTON_RIGHT 194
#define BUTTON_LEFT 34
#define BUTTON_VOL_DOWN 224
#define BUTTON_DOWN 168
#define BUTTON_VOL_UP 144
#define BUTTON_1 104
#define BUTTON_2 152
#define BUTTON_3 176
#define BUTTON_4 48
#define BUTTON_5 24
#define BUTTON_6 122
#define BUTTON_7 16
#define BUTTON_8 56
#define BUTTON_9 90
#define BUTTON_0 66
#define BUTTON_DELETE 82


#define DRAW_PAGE_TYPE_START 0x00
#define DRAW_PAGE_TYPE_END 0x01

#endif


